const News = require('../models/News')
const errorHandler = require('../utils/errorHandler')

module.exports.getAll = async function(req, res) {
  try {
    const news = await News.find({user: req.user.id})
    console.log(req.user.id)
    res.status(200).json(news);
  } catch (e) {
    errorHandler(res, e)
  }
}

module.exports.getById = async function(req, res) {
  try {
    const news = await News.findById(req.params.id)
    res.status(200).json(news)
  } catch (e) {
    errorHandler(res, e)
  }
}

module.exports.remove = async function(req, res) {
  try {
    await News.remove({_id: req.params.id})
    res.status(200).json({
      message: 'Новость удалена.'
    })
  } catch (e) {
    errorHandler(res, e)
  }
}

module.exports.create = async function(req, res) {
  const news = new News({
    name: req.body.name,
    position: req.body.position,
    imageSrc: req.file ? req.file.path : '',
    user: req.user.id
  })
  
  try {
    await news.save()
    res.status(201).json(news)
  } catch (e) {
    errorHandler(res, e)
  }
}

module.exports.update = async function(req, res) {
  const updated = {
    name: req.body.name,
    position: req.body.position
  }

  if (req.file) {
    updated.imageSrc = req.file.path
  }

  try {
    const news = await News.findOneAndUpdate(
      {_id: req.params.id},
      {$set: updated},
      {new: true}
    )
    res.status(200).json(news)
  } catch (e) {
    errorHandler(res, e)
  }
}