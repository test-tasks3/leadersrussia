const express = require('express')
const mongoose = require('mongoose')
const passport = require('passport')
const bodyParser = require('body-parser')
const authRoutes = require('../api/routes/auth')
const newsRoutes = require('../api/routes/news')
const usersRoutes = require('../api/routes/users')
const keys = require('../api/config/keys')
const app = express()

mongoose.connect(keys.mongoURI, { useNewUrlParser: true, useUnifiedTopology: true })
  .then((a) => console.log('MongoDB connected.',a.models) )
  .catch(error => console.log(error))

app.use(passport.initialize())
require('../api/middleware/passport')(passport)

app.use(require('morgan')('dev'))
app.use('/uploads', express.static('uploads'))
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())
app.use(require('cors')())

app.use('/api/auth', authRoutes)
app.use('/api/news', newsRoutes)
app.use('/api/users', usersRoutes)

module.exports = app;
